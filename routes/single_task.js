const User = require('../models/user');
const Task = require('../models/task');

module.exports = function(router) {
    const taskRoute = router.route('/tasks/:id');

    taskRoute.get(async (req, res) => {
        try {
            const task = await Task.findById(req.params.id).exec();
            if (!task) {
                return res.status(404).json({ message: 'Task not found', data: [] });
            }
            res.status(200).json({ message: 'Task retrieved successfully', data: task });
        } catch (error) {
            res.status(500).json({ message: 'Internal server error while retrieving task', data: [] });
        }
    });

    taskRoute.put(async (req, res) => {
        try {
            const task = await Task.findById(req.params.id).exec();
            if (!task) {
                return res.status(404).json({ message: 'Task not found', data: [] });
            }

            const updates = {};
            const fields = ['name', 'description', 'deadline', 'completed', 'assignedUser'];
            fields.forEach(field => {
                if (req.body.hasOwnProperty(field)) {
                    updates[field] = req.body[field];
                }
            });

            if (!updates.name) {
                return res.status(400).json({ message: 'Task name is required', data: [] });
            }

            if (!updates.deadline) {
                return res.status(400).json({ message: 'Task deadline is required', data: [] });
            }

            if (updates.assignedUser) {
                const user = await User.findById(updates.assignedUser).exec();
                if (!user) {
                    updates.assignedUser = '';
                    updates.assignedUserName = 'unassigned';
                } else {
                    updates.assignedUserName = user.name;
                    if (!task.completed) {
                        user.pendingTasks.pull(task._id);
                        await user.save();
                    }
                }
            } else {
                updates.assignedUser = '';
                updates.assignedUserName = 'unassigned';
            }

            const updatedTask = await Task.findByIdAndUpdate(task._id, updates, { new: true }).exec();
            res.status(200).json({ message: 'Task updated successfully', data: updatedTask });

        } catch (error) {
            res.status(500).json({ message: 'Internal server error while updating task', data: [] });
        }
    });

    taskRoute.delete(async (req, res) => {
        try {
            const task = await Task.findById(req.params.id).exec();
            if (!task) {
                return res.status(404).json({ message: 'Task not found', data: [] });
            }
            if (task.assignedUser && !task.completed) {
                const user = await User.findById(task.assignedUser).exec();
                if (user) {
                    user.pendingTasks.pull(task._id);
                    await user.save();
                }
            }
            await task.delete();
            res.status(200).json({ message: 'Task deleted successfully', data: [] });
        } catch (error) {
            res.status(500).json({ message: 'Internal server error while deleting task', data: [] });
        }
    });

    return router;
};

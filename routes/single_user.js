const User = require('../models/user.js');
const Task = require('../models/task.js');

module.exports = function(router) {
    const userRoute = router.route('/users/:id');

    userRoute.get(async (req, res) => {
        try {
            const user = await User.findById(req.params.id).exec();
            if (!user) {
                return res.status(404).json({ message: 'User not found', data: [] });
            }
            res.status(200).json({ message: 'User retrieved successfully', data: user });
        } catch (error) {
            res.status(500).json({ message: 'Error fetching user', data: [] });
        }
    });

    userRoute.put(async (req, res) => {
        try {
            const userToUpdate = await User.findById(req.params.id).exec();
            if (!userToUpdate) {
                return res.status(404).json({ message: 'User not found', data: [] });
            }

            // Validate the required fields
            if (!req.body.name) {
                return res.status(400).json({ message: 'Name is required', data: [] });
            }
            if (!req.body.email) {
                return res.status(400).json({ message: 'Email is required', data: [] });
            }

            // Check if the email is unique
            const emailExists = await User.findOne({ email: req.body.email }).exec();
            if (emailExists && emailExists._id.toString() !== userToUpdate._id.toString()) {
                return res.status(400).json({ message: 'Email is already in use', data: [] });
            }

            // Process any pending tasks
            const taskPromises = (req.body.pendingTasks || []).map(taskId => 
                Task.findById(taskId).exec()
            );
            const tasks = await Promise.all(taskPromises);
            const pendingTasks = tasks.filter(task => task !== null).map(task => task._id);

            // Update the user's information
            const updatedUser = await User.findByIdAndUpdate(
                userToUpdate._id,
                {
                    name: req.body.name,
                    email: req.body.email,
                    pendingTasks: pendingTasks
                },
                { new: true }
            ).exec();

            // Update related tasks to reflect changes in user
            const taskUpdates = updatedUser.pendingTasks.map(taskId => 
                Task.findByIdAndUpdate(taskId, {
                    assignedUser: updatedUser._id,
                    assignedUserName: updatedUser.name,
                    completed: false
                }, { new: true }).exec()
            );
            await Promise.all(taskUpdates);

            res.status(200).json({ message: 'User updated successfully', data: updatedUser });
        } catch (error) {
            res.status(500).json({ message: 'Error updating user', data: [] });
        }
    });

    userRoute.delete(async (req, res) => {
        try {
            const userToDelete = await User.findById(req.params.id).exec();
            if (!userToDelete) {
                return res.status(404).json({ message: 'User not found', data: [] });
            }
            await Task.updateMany({ assignedUser: userToDelete._id }, {
                assignedUser: "",
                assignedUserName: "unassigned"
            });
            await userToDelete.delete();
            res.status(200).json({ message: 'User deleted successfully', data: [] });
        } catch (error) {
            res.status(500).json({ message: 'Error deleting user', data: [] });
        }
    });

    return router;
};

const User = require('../models/user.js');
const Task = require('../models/task.js');

function parseQuery(query) {
    try {
        return JSON.parse(query);
    } catch (error) {
        return {};
    }
}

module.exports = function(router) {
    const route_task = router.route('/tasks');

    route_task.get(async (req, res) => {
        try {
            const where = parseQuery(req.query.where);
            const sort = parseQuery(req.query.sort);
            const select = parseQuery(req.query.select);
            const skip = parseInt(req.query.skip, 10) || 0;
            const limit = parseInt(req.query.limit, 10) || 0;

            const query = Task.find(where).sort(sort).select(select).skip(skip).limit(limit);
            const tasks = await query.exec();
            
            if (req.query.count === 'true') {
                res.status(200).json({ message: 'Number of tasks retrieved', data: tasks.length });
            } else {
                res.status(200).json({ message: 'Tasks retrieved successfully', data: tasks });
            }
        } catch (error) {
            res.status(500).json({ message: 'Error retrieving tasks', data: [] });
        }
    });

    route_task.post(async (req, res) => {
        if (!req.body.name) {
            return res.status(400).json({ message: 'Name is required', data: [] });
        }
        if (!req.body.deadline) {
            return res.status(400).json({ message: 'Deadline is required', data: [] });
        }

        const task = new Task({
            name: req.body.name,
            description: req.body.description || "",
            deadline: req.body.deadline,
            completed: req.body.completed || false,
            assignedUser: "",
            assignedUserName: "unassigned"
        });

        try {
            if (req.body.assignedUser) {
                const user = await User.findById(req.body.assignedUser).exec();
                if (user) {
                    task.assignedUser = user._id;
                    task.assignedUserName = user.name;
                    if (!task.completed) {
                        user.pendingTasks.push(task._id);
                        await user.save();
                    }
                }
            }

            const saved_task = await task.save();
            res.status(201).json({ message: 'Task created successfully', data: saved_task });

        } catch (error) {
            res.status(500).json({ message: 'Error creating task', data: [] });
        }
    });

    return router;
};

const User = require('../models/user.js');
const Task = require('../models/task.js');

function parseQueryParameter(param) {
    try {
        return JSON.parse(param);
    } catch (e) {
        return undefined;
    }
}

module.exports = function (router) {
    const route_user = router.route('/users');

    route_user.get(async (req, res) => {
        try {
            const where = parseQueryParameter(req.query.where) || {};
            const sort = parseQueryParameter(req.query.sort) || {};
            const select = parseQueryParameter(req.query.select) || '';
            const skip = parseInt(req.query.skip, 10) || 0;
            const limit = parseInt(req.query.limit, 10) || 0;

            const query = User.find(where).sort(sort).select(select).skip(skip).limit(limit);
            const users = await query.exec();

            if (req.query.count === 'true') {
                res.status(200).json({ message: 'Users count retrieved', data: users.length });
            } else {
                res.status(200).json({ message: 'Users retrieved successfully', data: users });
            }
        } catch (error) {
            res.status(500).json({ message: 'Error retrieving users', data: [] });
        }
    });

    route_user.post(async (req, res) => {
        if (!req.body.name) {
            return res.status(400).json({ message: 'Name is required', data: [] });
        }
        if (!req.body.email) {
            return res.status(400).json({ message: 'Email is required', data: [] });
        }

        try {
            const emailExists = await User.findOne({ email: req.body.email }).exec();
            if (emailExists) {
                return res.status(400).json({ message: 'Email already in use', data: [] });
            }

            const pendingTasks = await Promise.all((req.body.pendingTasks || []).map(taskId =>
                Task.findById(taskId).exec()
            ));

            const user = new User({
                name: req.body.name,
                email: req.body.email,
                pendingTasks: pendingTasks.filter(task => task !== null).map(task => task._id)
            });

            const saved_user = await user.save();
            
            // Update the tasks with the new user's ID
            const taskUpdates = saved_user.pendingTasks.map(taskId =>
                Task.findByIdAndUpdate(taskId, {
                    assignedUser: saved_user._id,
                    assignedUserName: saved_user.name,
                    completed: false
                }, { new: true }).exec()
            );
            await Promise.all(taskUpdates);

            res.status(201).json({ message: 'User created successfully', data: saved_user });
        } catch (error) {
            res.status(500).json({ message: 'Error creating user', data: [] });
        }
    });

    return router;
};
